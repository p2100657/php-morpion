<?php
require("services/db_service.php");

function home(){
    $btn = false;
    try{
        $pdo  = try_connect();
        $message = "<p>Connection success</p>";

        $btn = true;
    } catch (PDOException $e) {
        $message = '<p class="error">Could not connect: ' . $e->getMessage().'</p>';
    }

    require("templates/home.php");
}

function play($numGame){
    require("templates/game.php");
}

function chooseParti(){
    require("templates/homePlay.php");
}

function notFound($uri){
    require("templates/notFound.php");
}

?>
