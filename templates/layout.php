<!-- templates/baseLayout.php -->
<!DOCTYPE html>
<html lang="fr">
<head>
    <title><?php echo $title ?></title>
    <link href="assets/style.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div class="card">
    <?php echo $content ?>
    </div>
    <a href="/">GO HOME</a>
</body>
</html>